package ee.kuehnenagel.factory;

import ee.kuehnenagel.exception.UnsupportedFileTypeException;
import ee.kuehnenagel.service.parser.CsvParser;
import ee.kuehnenagel.service.parser.Parser;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ParserFactoryTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void shouldPass() {
        String fileName = "lol.csv";
        ParserFactory parserFactory = new ParserFactory();
        Parser parser = parserFactory.getParser(fileName);
        assertNotNull(parser);
        assertEquals(CsvParser.class, parser.getClass());
    }

    @Test
    public void shouldFail() {
        exceptionRule.expect(UnsupportedFileTypeException.class);
        exceptionRule.expectMessage("Available file extensions: .csv");
        String fileName = "lo.cv";
        ParserFactory parserFactory = new ParserFactory();
        parserFactory.getParser(fileName);
    }

}