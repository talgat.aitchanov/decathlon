package ee.kuehnenagel.util;

import ee.kuehnenagel.exception.ApplicationException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class NumberUtilTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void shouldFailOnInitialization() {
        exceptionRule.expect(ApplicationException.class);
        exceptionRule.expectMessage("Util class shouldn't be initialized");
        new NumberUtil();
    }

}