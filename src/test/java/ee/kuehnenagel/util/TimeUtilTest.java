package ee.kuehnenagel.util;

import ee.kuehnenagel.exception.ApplicationException;
import ee.kuehnenagel.exception.TimeFormatException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class TimeUtilTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void minutesToSeconds() {
        double d = TimeUtil.convert("01:01.01");
        assertEquals(61.01, d, 2);
    }

    @Test
    public void secondWithMillisecondsToSeconds() {
        double d = TimeUtil.convert("01.01");
        assertEquals(1.01, d, 2);
    }

    @Test(expected = TimeFormatException.class)
    public void shouldThrowTimeFormatException() {
        TimeUtil.convert("/0.01");
    }

    @Test
    public void shouldThrowTimeFormatException2() {
        exceptionRule.expect(TimeFormatException.class);
        exceptionRule.expectMessage("Incorrect format; Example: '1:01.00' or '1.00'");
        TimeUtil.convert("01");
    }

    @Test
    public void shouldFailOnInitialization() {
        exceptionRule.expect(ApplicationException.class);
        exceptionRule.expectMessage("Util class shouldn't be initialized");
        new TimeUtil();
    }

}