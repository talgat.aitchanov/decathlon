package ee.kuehnenagel.service.parser;

import ee.kuehnenagel.domain.AthleteResult;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class CsvParserTest {

    @Test
    public void shouldParse() {
        String validLine = "Foo Bar;13.43;4.35;8.64;1.50;66.06;19.05;24.89;2.20;33.48;6:51.01";
        Parser parser = CsvParser.getInstance();
        List<AthleteResult> athleteResultList = parser.parse(List.of(validLine));
        assertEquals(1, athleteResultList.size());
    }

    @Test
    public void shouldParseAndReturnEmpty() {
        String validLine = "Foo Bar;13.43;4.35;8.64;1.50;66.06;19.05;24.89;2.20;33.48";
        Parser parser = CsvParser.getInstance();
        List<AthleteResult> athleteResultList = parser.parse(List.of(validLine));
        assertEquals(1, athleteResultList.size());
        assertNull(athleteResultList.get(0).getOneHundredMeter());
        assertNull(athleteResultList.get(0).getAthlete().getName());
    }

    @Test
    public void shouldParseAndReturnZero() {
        String validLine = " ";
        Parser parser = CsvParser.getInstance();
        List<AthleteResult> athleteResultList = parser.parse(List.of(validLine));
        assertEquals(0, athleteResultList.size());
    }

}