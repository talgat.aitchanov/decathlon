package ee.kuehnenagel.service;

import ee.kuehnenagel.domain.Athlete;
import ee.kuehnenagel.domain.AthleteResult;
import ee.kuehnenagel.event.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ResultsCalculatorTest {

    private List<AthleteResult> athleteResultList;

    @Before
    public void setUp() {
        athleteResultList = new ArrayList<>();
        Athlete athlete = new Athlete();
        athlete.setName("A");
        athlete.setSurname("B");
        AthleteResult athleteResult = new AthleteResult.AthleteResultBuilder(athlete)
                .oneHundredMeter(new OneHundredMeter(13.43))
                .longJump(new LongJump(4.35))
                .shotPut(new ShotPut(8.64))
                .highJump(new HighJump(1.50))
                .fourHundredMeter(new FourHundredMeter(66.06))
                .oneHundredTenMeterHurdles(new OneHundredTenMeterHurdles(19.05))
                .diskusThrow(new DiskusThrow(24.89))
                .poleVault(new PoleVault(2.20))
                .javelinThrow(new JavelinThrow(33.48))
                .oneThousandFiveHundredMeter(new OneThousandFiveHundredMeter(411.01))
                .build();
        athleteResultList.add(athleteResult);
    }

    @Test
    public void shouldCalculate() {
        ResultsCalculator resultsCalculator = new ResultsCalculator();
        List<AthleteResult> calculate = resultsCalculator.calculate(athleteResultList);
        AthleteResult athleteResult = calculate.get(0);
        assertEquals(2267, athleteResult.getTotalScore());
        assertEquals("A B", athleteResult.getAthlete().getFullName());
    }

}