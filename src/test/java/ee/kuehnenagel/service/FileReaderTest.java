package ee.kuehnenagel.service;

import ee.kuehnenagel.exception.FileReaderException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static org.junit.Assert.*;

public class FileReaderTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void shouldRead() {
        String fileName = "results.csv";
        FileReader fileReader = new FileReader();
        List<String> strings = fileReader.retrieveData(fileName);
        assertNotNull(strings);
        assertTrue(strings.size() > 0);
        assertEquals(7, strings.size());
    }

    @Test
    public void shouldFailFileNotFound() {
        exceptionRule.expect(FileReaderException.class);
        exceptionRule.expectMessage("file not found!");
        String fileName = "results";
        FileReader fileReader = new FileReader();
        fileReader.retrieveData(fileName);
    }

}