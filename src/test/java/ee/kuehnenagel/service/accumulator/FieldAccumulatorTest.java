package ee.kuehnenagel.service.accumulator;

import ee.kuehnenagel.enums.UnitType;
import ee.kuehnenagel.event.Event;
import ee.kuehnenagel.event.LongJump;
import ee.kuehnenagel.event.ShotPut;
import ee.kuehnenagel.util.NumberUtil;
import org.junit.Test;

import static org.junit.Assert.*;

public class FieldAccumulatorTest {

    @Test
    public void shouldCalculateLongJumpEvent() {
        Accumulator fieldAccumulator = FieldAccumulator.getInstance();
        double centimeter = NumberUtil.meterToCentimeter(5.00);
        Event longJumpEvent = new LongJump(centimeter);
        int longJumpEventPoints = fieldAccumulator.calculate(longJumpEvent);
        assertSame(UnitType.METER, longJumpEvent.getEventType());
        assertTrue(longJumpEventPoints > 0);
        assertEquals(382, longJumpEventPoints);
    }

    @Test
    public void shouldCalculateShotPutEvent() {
        Accumulator fieldAccumulator = FieldAccumulator.getInstance();
        Event shotPutEvent = new ShotPut(9.22);
        int shotPutEventPoints = fieldAccumulator.calculate(shotPutEvent);
        assertSame(UnitType.METER, shotPutEvent.getEventType());
        assertTrue(shotPutEventPoints > 0);
        assertEquals(439, shotPutEventPoints);
    }

}