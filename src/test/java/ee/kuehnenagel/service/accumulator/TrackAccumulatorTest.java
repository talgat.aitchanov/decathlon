package ee.kuehnenagel.service.accumulator;

import ee.kuehnenagel.enums.UnitType;
import ee.kuehnenagel.event.Event;
import ee.kuehnenagel.event.OneHundredMeter;
import ee.kuehnenagel.event.OneThousandFiveHundredMeter;
import ee.kuehnenagel.util.TimeUtil;
import org.junit.Test;

import static org.junit.Assert.*;

public class TrackAccumulatorTest {

    @Test
    public void shouldCalculateOneHundredMeterEvent() {
        Accumulator trackAccumulator = TrackAccumulator.getInstance();
        double seconds = 12.61;
        Event oneHundredMeterEvent = new OneHundredMeter(seconds);
        int oneHundredMeterEventPoints = trackAccumulator.calculate(oneHundredMeterEvent);
        assertSame(UnitType.SECOND, oneHundredMeterEvent.getEventType());
        assertTrue(oneHundredMeterEventPoints > 0);
        assertEquals(536, oneHundredMeterEventPoints);
    }

    @Test
    public void shouldCalculateOneThousandFiveHundredMeterEvent() {
        Accumulator trackAccumulator = TrackAccumulator.getInstance();
        double seconds = TimeUtil.convert("5:25.72");
        Event oneThousandFiveHundredMeterEvent = new OneThousandFiveHundredMeter(seconds);
        int oneThousandFiveHundredMeterEventPoints = trackAccumulator.calculate(oneThousandFiveHundredMeterEvent);
        assertSame(UnitType.SECOND, oneThousandFiveHundredMeterEvent.getEventType());
        assertTrue(oneThousandFiveHundredMeterEventPoints > 0);
        assertEquals(325.72, seconds, 2);
        assertEquals(421, oneThousandFiveHundredMeterEventPoints);
    }

}