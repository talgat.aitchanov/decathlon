package ee.kuehnenagel.service.accumulator;

import ee.kuehnenagel.event.Event;

public class FieldAccumulator implements Accumulator {

    private static FieldAccumulator instance;

    private FieldAccumulator() {
    }

    public static FieldAccumulator getInstance() {
        if (instance == null) {
            synchronized (FieldAccumulator.class) {
                if (instance == null) {
                    instance = new FieldAccumulator();
                }
            }
        }
        return instance;
    }

    @Override
    public int calculate(Event event) {
        double factor = event.getA();
        double base = event.getP() - event.getB();
        double power = event.getC();
        double result = factor * Math.pow(base, power);
        return (int) result;
    }

}
