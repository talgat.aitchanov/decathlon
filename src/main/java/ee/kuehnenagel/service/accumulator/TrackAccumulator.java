package ee.kuehnenagel.service.accumulator;

import ee.kuehnenagel.event.Event;

public class TrackAccumulator implements Accumulator {

    private static TrackAccumulator instance;

    private TrackAccumulator() {
    }

    public static TrackAccumulator getInstance() {
        if (instance == null) {
            synchronized (TrackAccumulator.class) {
                if (instance == null) {
                    instance = new TrackAccumulator();
                }
            }
        }
        return instance;
    }

    @Override
    public int calculate(Event event) {
        double factor = event.getA();
        double base = event.getB() - event.getP();
        double power = event.getC();
        double result = factor * Math.pow(base, power);
        return (int) result;
    }

}
