package ee.kuehnenagel.service.accumulator;

import ee.kuehnenagel.event.Event;

public interface Accumulator {

    int calculate(Event event);

}
