package ee.kuehnenagel.service.parser;

import ee.kuehnenagel.domain.Athlete;
import ee.kuehnenagel.domain.AthleteResult;
import ee.kuehnenagel.event.*;
import ee.kuehnenagel.util.NumberUtil;
import ee.kuehnenagel.util.StringUtil;
import ee.kuehnenagel.util.TimeUtil;

import java.util.List;
import java.util.stream.Collectors;

public class CsvParser implements Parser {

    private static final String LINE_TOKENIZER = ";";
    private static final String NAME_TOKENIZER = " ";
    private static final int MIN_ARRAY_LENGTH = 11;

    private static final int NAME_AND_SURNAME_IDX = 0;
    private static final int ONE_HUNDRED_METER_RESULT_VALUE_IDX = 1;
    private static final int LONG_JUMP_RESULT_VALUE_IDX = 2;
    private static final int SHOT_PUT_RESULT_VALUE_IDX = 3;
    private static final int HIGH_JUMP_RESULT_VALUE_IDX = 4;
    private static final int FOUR_HUNDRED_METER_RESULT_VALUE_IDX = 5;
    private static final int ONE_HUNDRED_TEN_METER_HURDLES_RESULT_VALUE_IDX = 6;
    private static final int DISKUS_THROW_RESULT_VALUE_IDX = 7;
    private static final int POLE_VAULT_RESULT_VALUE_IDX = 8;
    private static final int JAVELIN_THROW_RESULT_VALUE_IDX = 9;
    private static final int ONE_THOUSAND_FIVE_HUNDRED_METER_RESULT_VALUE_IDX = 10;

    private static CsvParser instance;

    private CsvParser() {
    }

    public static CsvParser getInstance() {
        if (instance == null) {
            synchronized (CsvParser.class) {
                if (instance == null) {
                    instance = new CsvParser();
                }
            }
        }
        return instance;
    }

    @Override
    public List<AthleteResult> parse(List<String> lines) {
        return lines
                .stream()
                .filter(StringUtil::isNotEmpty)
                .map(this::lineToAthleteResult)
                .collect(Collectors.toList());
    }

    private AthleteResult lineToAthleteResult(String line) {
        String[] split = line.split(LINE_TOKENIZER);
        if (split.length == MIN_ARRAY_LENGTH) {
            return createAthleteResult(split);
        }
        //TODO: probably throwing an exception is better
        return new AthleteResult
                .AthleteResultBuilder(new Athlete())
                .build();
    }

    private AthleteResult createAthleteResult(String[] split) {
        Athlete athlete = createAthlete(split[NAME_AND_SURNAME_IDX]);

        String oneHundredMeterResultValue = split[ONE_HUNDRED_METER_RESULT_VALUE_IDX];
        OneHundredMeter oneHundredMeter = new OneHundredMeter(TimeUtil.convert(oneHundredMeterResultValue));

        String longJumpResultValue = split[LONG_JUMP_RESULT_VALUE_IDX];
        LongJump longJump = new LongJump(
                NumberUtil.meterToCentimeter(
                        Double.parseDouble(longJumpResultValue)
                )
        );

        String shotPutResultValue = split[SHOT_PUT_RESULT_VALUE_IDX];
        ShotPut shotPut = new ShotPut(Double.parseDouble(shotPutResultValue));

        String highJumpResultValue = split[HIGH_JUMP_RESULT_VALUE_IDX];
        HighJump highJump = new HighJump(
                NumberUtil.meterToCentimeter(
                        Double.parseDouble(highJumpResultValue)
                )
        );

        String fourHundredMeterResultValue = split[FOUR_HUNDRED_METER_RESULT_VALUE_IDX];
        FourHundredMeter fourHundredMeter = new FourHundredMeter(TimeUtil.convert(fourHundredMeterResultValue));

        String oneHundredTenMeterHurdlesResultValue = split[ONE_HUNDRED_TEN_METER_HURDLES_RESULT_VALUE_IDX];
        OneHundredTenMeterHurdles oneHundredTenMeterHurdles = new OneHundredTenMeterHurdles(TimeUtil.convert(oneHundredTenMeterHurdlesResultValue));

        String diskusThrowResultValue = split[DISKUS_THROW_RESULT_VALUE_IDX];
        DiskusThrow diskusThrow = new DiskusThrow(Double.parseDouble(diskusThrowResultValue));

        String poleVaultResultValue = split[POLE_VAULT_RESULT_VALUE_IDX];
        PoleVault poleVault = new PoleVault(
                NumberUtil.meterToCentimeter(
                        Double.parseDouble(poleVaultResultValue)
                )
        );

        String javelinThrowResultValue = split[JAVELIN_THROW_RESULT_VALUE_IDX];
        JavelinThrow javelinThrow = new JavelinThrow(Double.parseDouble(javelinThrowResultValue));

        String oneThousandFiveHundredMeterResultValue = split[ONE_THOUSAND_FIVE_HUNDRED_METER_RESULT_VALUE_IDX];
        OneThousandFiveHundredMeter oneThousandFiveHundredMeter = new OneThousandFiveHundredMeter(TimeUtil.convert(oneThousandFiveHundredMeterResultValue));

        return new AthleteResult.AthleteResultBuilder(athlete)
                .oneHundredMeter(oneHundredMeter)
                .longJump(longJump)
                .shotPut(shotPut)
                .highJump(highJump)
                .fourHundredMeter(fourHundredMeter)
                .oneHundredTenMeterHurdles(oneHundredTenMeterHurdles)
                .diskusThrow(diskusThrow)
                .poleVault(poleVault)
                .javelinThrow(javelinThrow)
                .oneThousandFiveHundredMeter(oneThousandFiveHundredMeter)
                .build();
    }

    private Athlete createAthlete(String nameAndSurname) {
        int nameIdx = 0;
        int surnameIdx = 1;
        String[] split = nameAndSurname.split(NAME_TOKENIZER);
        String name = split[nameIdx];
        String surname = split[surnameIdx];
        Athlete athlete = new Athlete();
        athlete.setName(name);
        athlete.setSurname(surname);
        return athlete;
    }

}
