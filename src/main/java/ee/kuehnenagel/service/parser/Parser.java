package ee.kuehnenagel.service.parser;

import ee.kuehnenagel.domain.AthleteResult;

import java.util.List;

public interface Parser {

    List<AthleteResult> parse(List<String> lines);

}
