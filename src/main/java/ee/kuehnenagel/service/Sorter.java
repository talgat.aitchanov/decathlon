package ee.kuehnenagel.service;

import ee.kuehnenagel.domain.AthleteResult;

import java.util.ArrayList;
import java.util.List;

public class Sorter {

    public  List<AthleteResult> sortByPoints(List<AthleteResult> calculatedAthleteResultList) {
        List<AthleteResult> sortedCalculatedAthleteResultList = new ArrayList<>(calculatedAthleteResultList);
        sortedCalculatedAthleteResultList.sort(new AthleteResult.SortByTotalScore());
        return sortedCalculatedAthleteResultList;
    }

}
