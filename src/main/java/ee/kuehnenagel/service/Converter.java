package ee.kuehnenagel.service;

import ee.kuehnenagel.domain.AthleteResult;

import java.util.List;

public class Converter {

    public String athleteResultToXmlString(List<AthleteResult> athleteResultList) {
        String tag = "AthleteResults";
        StringBuilder sb = new StringBuilder("<" + tag + ">");
        athleteResultList.forEach(athleteResult -> sb.append(athleteResult.toString()));
        sb.append("</" + tag + ">");
        return sb.toString();
    }

}
