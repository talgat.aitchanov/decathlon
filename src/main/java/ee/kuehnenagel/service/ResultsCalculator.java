package ee.kuehnenagel.service;

import ee.kuehnenagel.domain.AthleteResult;
import ee.kuehnenagel.event.Event;
import ee.kuehnenagel.factory.AccumulatorFactory;

import java.util.ArrayList;
import java.util.List;

public class ResultsCalculator {

    public List<AthleteResult> calculate(List<AthleteResult> athleteResultList) {
        List<AthleteResult> calculatedAthleteResultList = new ArrayList<>(athleteResultList);
        AccumulatorFactory accumulatorFactory = new AccumulatorFactory();
        calculatedAthleteResultList
                .forEach(athleteResult -> {
                            int points = summarizePoints(athleteResult, accumulatorFactory);
                            athleteResult.setTotalScore(points);
                        }
                );
        return calculatedAthleteResultList;
    }

    private int summarizePoints(AthleteResult athleteResult, AccumulatorFactory accumulatorFactory) {
        return calculateResult(athleteResult.getOneHundredMeter(), accumulatorFactory)
                + calculateResult(athleteResult.getLongJump(), accumulatorFactory)
                + calculateResult(athleteResult.getShotPut(), accumulatorFactory)
                + calculateResult(athleteResult.getHighJump(), accumulatorFactory)
                + calculateResult(athleteResult.getFourHundredMeter(), accumulatorFactory)
                + calculateResult(athleteResult.getOneHundredTenMeterHurdles(), accumulatorFactory)
                + calculateResult(athleteResult.getDiskusThrow(), accumulatorFactory)
                + calculateResult(athleteResult.getPoleVault(), accumulatorFactory)
                + calculateResult(athleteResult.getJavelinThrow(), accumulatorFactory)
                + calculateResult(athleteResult.getOneThousandFiveHundredMeter(), accumulatorFactory);
    }

    private int calculateResult(Event event, AccumulatorFactory accumulatorFactory) {
        return accumulatorFactory
                .getAccumulator(
                        event.getEventType()
                )
                .calculate(event);
    }

}
