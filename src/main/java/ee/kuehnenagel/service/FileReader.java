package ee.kuehnenagel.service;

import ee.kuehnenagel.exception.FileReaderException;
import ee.kuehnenagel.exception.UnsupportedFileTypeException;
import ee.kuehnenagel.util.StringUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

public class FileReader {

    public List<String> retrieveData(String fileName) {
        File file = getFile(fileName);
        return getLines(file);
    }

    private File getFile(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new FileReaderException("file not found!");
        } else {
            return new File(resource.getFile());
        }
    }

    private List<String> getLines(File file) {
        try (java.io.FileReader reader = new java.io.FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {
            return br
                    .lines()
                    .filter(StringUtil::isNotEmpty)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsupportedFileTypeException("the file cannot be read", e);
        }
    }

}
