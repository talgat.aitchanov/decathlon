package ee.kuehnenagel.service;

import ee.kuehnenagel.domain.AthleteResult;

import java.util.ArrayList;
import java.util.List;

public class Allocator {

    public List<AthleteResult> allocatePlaces(List<AthleteResult> sortedCalculatedAthleteResultList) {
        List<AthleteResult> allocatedPlacesAthleteResultList = new ArrayList<>(sortedCalculatedAthleteResultList);
        int points = 0;
        int startIdx = 0;
        int listSize = allocatedPlacesAthleteResultList.size();

        for (int currentIndex = 0; currentIndex < listSize; currentIndex++) {
            int currentPoints = allocatedPlacesAthleteResultList.get(currentIndex).getTotalScore();
            //arrange places for the beginning and middle data
            if (points != currentPoints) {
                points = currentPoints;

                //skip for the first entry
                boolean isFirst = currentIndex == 0;
                if (isFirst) {
                    continue;
                }

                //arrange places for everything besides ending
                int fromPlace = startIdx + 1;
                for (int i = startIdx; i < currentIndex; i++) {
                    if (fromPlace == currentIndex) {
                        allocatedPlacesAthleteResultList.get(i).setPlace(fromPlace + "");
                    } else {
                        allocatedPlacesAthleteResultList.get(i).setPlace(fromPlace + "-" + currentIndex);
                    }
                }
                //pointer where should next loop start
                startIdx = currentIndex;
            }

            //arrange places for the ending
            if (currentIndex == listSize - 1) {
                int fromPlace = startIdx + 1;
                for (int i = startIdx; i <= currentIndex; i++) {
                    if (startIdx == currentIndex) {
                        allocatedPlacesAthleteResultList.get(i).setPlace(fromPlace + "");
                    } else {
                        allocatedPlacesAthleteResultList.get(i).setPlace(fromPlace + "-" + listSize);
                    }
                }
            }
        }
        return allocatedPlacesAthleteResultList;
    }

}
