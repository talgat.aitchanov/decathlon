package ee.kuehnenagel.service;

import ee.kuehnenagel.exception.FileWriteException;

import java.io.BufferedWriter;
import java.io.IOException;

public class FileWriter {

    private static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

    public void writeToXml(String xmlString, String filename) {
        xmlString = XML_HEADER + xmlString;
        try (java.io.FileWriter fileWriter = new java.io.FileWriter(filename);
             BufferedWriter writer = new BufferedWriter(fileWriter)) {
            writer.write(xmlString);
        } catch (IOException e) {
            throw new FileWriteException("Cannot write to file", e);
        }
    }

}
