package ee.kuehnenagel.exception;

public class FileWriteException extends RuntimeException {

    public FileWriteException(String message, Throwable e) {
        super(message, e);
    }

}
