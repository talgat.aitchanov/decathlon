package ee.kuehnenagel.exception;

public class FileReaderException extends RuntimeException {

    public FileReaderException(String message) {
        super(message);
    }

}
