package ee.kuehnenagel.exception;

public class TimeFormatException extends RuntimeException {

    public TimeFormatException(Throwable cause) {
        super(cause);
    }

    public TimeFormatException(String message) {
        super(message);
    }

}
