package ee.kuehnenagel;

import ee.kuehnenagel.domain.AthleteResult;
import ee.kuehnenagel.factory.ParserFactory;
import ee.kuehnenagel.service.*;
import ee.kuehnenagel.service.parser.Parser;

import java.util.List;

public class DecathlonMain {

    public static void main(String[] args) {
        String sourceFile = "results.csv";
        List<AthleteResult> athleteResultList = fileToList(sourceFile);

        List<AthleteResult> allocatedPlacesAthleteResultList = processAthleteResult(athleteResultList);

        String outcomeFile = "result.xml";
        writeToFile(allocatedPlacesAthleteResultList, outcomeFile);
    }

    private static List<AthleteResult> fileToList(String fileName) {
        ParserFactory parserFactory = new ParserFactory();
        Parser parser = parserFactory.getParser(fileName);
        FileReader fileReader = new FileReader();
        List<String> strings = fileReader.retrieveData(fileName);
        return parser.parse(strings);
    }

    private static List<AthleteResult> processAthleteResult(List<AthleteResult> athleteResultList) {
        ResultsCalculator resultsCalculator = new ResultsCalculator();
        List<AthleteResult> calculatedAthleteResultList = resultsCalculator.calculate(athleteResultList);
        Sorter sorter = new Sorter();
        List<AthleteResult> sortedCalculatedAthleteResultList = sorter.sortByPoints(calculatedAthleteResultList);
        Allocator allocator = new Allocator();
        return allocator.allocatePlaces(sortedCalculatedAthleteResultList);
    }

    private static void writeToFile(List<AthleteResult> allocatedPlacesAthleteResultList, String fileName) {
        Converter converter = new Converter();
        String xmlString = converter.athleteResultToXmlString(allocatedPlacesAthleteResultList);
        FileWriter fileWriter = new FileWriter();
        fileWriter.writeToXml(xmlString, fileName);
    }

}
