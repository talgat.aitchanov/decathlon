package ee.kuehnenagel.factory;

import ee.kuehnenagel.service.accumulator.Accumulator;
import ee.kuehnenagel.service.accumulator.FieldAccumulator;
import ee.kuehnenagel.service.accumulator.TrackAccumulator;
import ee.kuehnenagel.enums.UnitType;
import ee.kuehnenagel.exception.ApplicationException;

import static ee.kuehnenagel.enums.UnitType.METER;
import static ee.kuehnenagel.enums.UnitType.SECOND;

public class AccumulatorFactory {

    public Accumulator getAccumulator(UnitType unitType) {
        if (SECOND == unitType) {
            return TrackAccumulator.getInstance();
        }
        if (METER == unitType) {
            return FieldAccumulator.getInstance();
        }
        throw new ApplicationException("unitType must be METER or SECOND. Actual " + unitType);
    }

}
