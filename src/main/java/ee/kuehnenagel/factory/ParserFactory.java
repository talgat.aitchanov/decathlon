package ee.kuehnenagel.factory;

import ee.kuehnenagel.exception.UnsupportedFileTypeException;
import ee.kuehnenagel.service.parser.CsvParser;
import ee.kuehnenagel.service.parser.Parser;

public class ParserFactory {

    private static final String CSV_EXTENSION = ".csv";

    public Parser getParser(String fileName) {
        boolean isSupportedExtension = fileName.endsWith(CSV_EXTENSION);
        if (isSupportedExtension) {
            return CsvParser.getInstance();
        }
        throw new UnsupportedFileTypeException("Available file extensions: " + CSV_EXTENSION);
    }

}
