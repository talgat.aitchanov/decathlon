package ee.kuehnenagel.util;

import ee.kuehnenagel.exception.ApplicationException;
import ee.kuehnenagel.exception.TimeFormatException;

public class TimeUtil {

    private static final String MINUTE_DELIMITER = ":";
    private static final String MILLISECOND_DELIMITER = ".";

    public TimeUtil() {
        throw new ApplicationException("Util class shouldn't be initialized");
    }

    public static double convert(String time) {
        try {
            if (time.contains(MINUTE_DELIMITER)) {
                return convertMinutes(time);
            } else if (time.contains(MILLISECOND_DELIMITER)) {
                return convertSeconds(time);
            }
        } catch (Exception e) {
            throw new TimeFormatException(e);
        }
        throw new TimeFormatException("Incorrect format; Example: '1:01.00' or '1.00'");
    }

    private static double convertMinutes(String time) {
        String[] split = time.split(MINUTE_DELIMITER);
        String minutesStr = split[0];
        int minutes = Integer.parseInt(minutesStr);
        int seconds = minutes * 60;
        String secondsStr = split[1];
        double convertedSeconds = convertSeconds(secondsStr);
        return convertedSeconds + seconds;
    }

    private static double convertSeconds(String time) {
        return Double.parseDouble(time);
    }

}
