package ee.kuehnenagel.util;

import ee.kuehnenagel.exception.ApplicationException;

public class StringUtil {

    public StringUtil() {
        throw new ApplicationException("Util class shouldn't be initialized");
    }

    public static boolean isNotEmpty(String line) {
        return !"".equals(
                line.trim()
        );
    }

}
