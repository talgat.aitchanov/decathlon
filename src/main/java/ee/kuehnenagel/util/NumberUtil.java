package ee.kuehnenagel.util;

import ee.kuehnenagel.exception.ApplicationException;

public class NumberUtil {

    private static final int FACTOR = 100;

    public NumberUtil() {
        throw new ApplicationException("Util class shouldn't be initialized");
    }

    public static double meterToCentimeter(double meter) {
        return meter * FACTOR;
    }

}
