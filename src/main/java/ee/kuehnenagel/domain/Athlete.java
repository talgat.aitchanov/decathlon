package ee.kuehnenagel.domain;

public class Athlete {
    private String name;
    private String surname;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public String getFullName() {
        return String.join(" ", name, surname);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Athlete athlete = (Athlete) o;
        return name.equals(athlete.name) &&
                surname.equals(athlete.surname);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (name == null ? 0 : name.hashCode());
        hash = 31 * hash + (surname == null ? 0 : surname.hashCode());
        return hash;
    }

    @Override
    public String toString() {
        return "<Fullname>" + getFullName() + "</Fullname>";
    }
}
