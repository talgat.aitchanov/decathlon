package ee.kuehnenagel.domain;

import ee.kuehnenagel.event.*;

import java.util.Comparator;

public final class AthleteResult {

    private Athlete athlete;
    private OneHundredMeter oneHundredMeter;
    private LongJump longJump;
    private ShotPut shotPut;
    private HighJump highJump;
    private FourHundredMeter fourHundredMeter;
    private OneHundredTenMeterHurdles oneHundredTenMeterHurdles;
    private DiskusThrow diskusThrow;
    private PoleVault poleVault;
    private JavelinThrow javelinThrow;
    private OneThousandFiveHundredMeter oneThousandFiveHundredMeter;
    private int totalScore;
    private String place;

    private AthleteResult(AthleteResultBuilder athleteResultBuilder) {
        this.athlete = athleteResultBuilder.athlete;
        this.oneHundredMeter = athleteResultBuilder.oneHundredMeter;
        this.longJump = athleteResultBuilder.longJump;
        this.shotPut = athleteResultBuilder.shotPut;
        this.highJump = athleteResultBuilder.highJump;
        this.fourHundredMeter = athleteResultBuilder.fourHundredMeter;
        this.oneHundredTenMeterHurdles = athleteResultBuilder.oneHundredTenMeterHurdles;
        this.diskusThrow = athleteResultBuilder.diskusThrow;
        this.poleVault = athleteResultBuilder.poleVault;
        this.javelinThrow = athleteResultBuilder.javelinThrow;
        this.oneThousandFiveHundredMeter = athleteResultBuilder.oneThousandFiveHundredMeter;
    }

    public Athlete getAthlete() {
        return athlete;
    }

    public OneHundredMeter getOneHundredMeter() {
        return oneHundredMeter;
    }

    public LongJump getLongJump() {
        return longJump;
    }

    public ShotPut getShotPut() {
        return shotPut;
    }

    public HighJump getHighJump() {
        return highJump;
    }

    public FourHundredMeter getFourHundredMeter() {
        return fourHundredMeter;
    }

    public OneHundredTenMeterHurdles getOneHundredTenMeterHurdles() {
        return oneHundredTenMeterHurdles;
    }

    public DiskusThrow getDiskusThrow() {
        return diskusThrow;
    }

    public PoleVault getPoleVault() {
        return poleVault;
    }

    public JavelinThrow getJavelinThrow() {
        return javelinThrow;
    }

    public OneThousandFiveHundredMeter getOneThousandFiveHundredMeter() {
        return oneThousandFiveHundredMeter;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AthleteResult that = (AthleteResult) o;
        return athlete.equals(that.athlete) &&
                oneHundredMeter.equals(that.oneHundredMeter) &&
                longJump.equals(that.longJump) &&
                shotPut.equals(that.shotPut) &&
                highJump.equals(that.highJump) &&
                fourHundredMeter.equals(that.fourHundredMeter) &&
                oneHundredTenMeterHurdles.equals(that.oneHundredTenMeterHurdles) &&
                diskusThrow.equals(that.diskusThrow) &&
                poleVault.equals(that.poleVault) &&
                javelinThrow.equals(that.javelinThrow) &&
                oneThousandFiveHundredMeter.equals(that.oneThousandFiveHundredMeter);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + athlete.hashCode();
        hash = 31 * hash + (oneHundredMeter == null ? 0 : oneHundredMeter.hashCode());
        hash = 31 * hash + (longJump == null ? 0 : longJump.hashCode());
        hash = 31 * hash + (shotPut == null ? 0 : shotPut.hashCode());
        hash = 31 * hash + (highJump == null ? 0 : highJump.hashCode());
        hash = 31 * hash + (fourHundredMeter == null ? 0 : fourHundredMeter.hashCode());
        hash = 31 * hash + (oneHundredTenMeterHurdles == null ? 0 : oneHundredTenMeterHurdles.hashCode());
        hash = 31 * hash + (diskusThrow == null ? 0 : diskusThrow.hashCode());
        hash = 31 * hash + (poleVault == null ? 0 : poleVault.hashCode());
        hash = 31 * hash + (javelinThrow == null ? 0 : javelinThrow.hashCode());
        hash = 31 * hash + (oneThousandFiveHundredMeter == null ? 0 : oneThousandFiveHundredMeter.hashCode());
        return hash;
    }

    @Override
    public String toString() {
        return "<AthleteResult>"
                + "<Place>" + getPlace() + "</Place>"
                + "<TotalScore>" + getTotalScore() + "</TotalScore>"
                + athlete.toString()
                + oneHundredMeter.toString()
                + longJump.toString()
                + shotPut.toString()
                + highJump.toString()
                + fourHundredMeter.toString()
                + oneHundredTenMeterHurdles.toString()
                + diskusThrow.toString()
                + poleVault.toString()
                + javelinThrow.toString()
                + oneThousandFiveHundredMeter.toString()
                + "</AthleteResult>";
    }

    public static class SortByTotalScore implements Comparator<AthleteResult> {
        public int compare(AthleteResult a, AthleteResult b) {
            return b.getTotalScore() - a.getTotalScore();
        }
    }

    public static class AthleteResultBuilder {

        private final Athlete athlete;
        private OneHundredMeter oneHundredMeter;
        private LongJump longJump;
        private ShotPut shotPut;
        private HighJump highJump;
        private FourHundredMeter fourHundredMeter;
        private OneHundredTenMeterHurdles oneHundredTenMeterHurdles;
        private DiskusThrow diskusThrow;
        private PoleVault poleVault;
        private JavelinThrow javelinThrow;
        private OneThousandFiveHundredMeter oneThousandFiveHundredMeter;

        public AthleteResultBuilder(Athlete athlete) {
            this.athlete = athlete;
        }

        public AthleteResultBuilder oneHundredMeter(OneHundredMeter oneHundredMeter) {
            this.oneHundredMeter = oneHundredMeter;
            return this;
        }

        public AthleteResultBuilder longJump(LongJump longJump) {
            this.longJump = longJump;
            return this;
        }

        public AthleteResultBuilder shotPut(ShotPut shotPut) {
            this.shotPut = shotPut;
            return this;
        }

        public AthleteResultBuilder highJump(HighJump highJump) {
            this.highJump = highJump;
            return this;
        }

        public AthleteResultBuilder fourHundredMeter(FourHundredMeter fourHundredMeter) {
            this.fourHundredMeter = fourHundredMeter;
            return this;
        }

        public AthleteResultBuilder oneHundredTenMeterHurdles(OneHundredTenMeterHurdles oneHundredTenMeterHurdles) {
            this.oneHundredTenMeterHurdles = oneHundredTenMeterHurdles;
            return this;
        }

        public AthleteResultBuilder diskusThrow(DiskusThrow diskusThrow) {
            this.diskusThrow = diskusThrow;
            return this;
        }

        public AthleteResultBuilder poleVault(PoleVault poleVault) {
            this.poleVault = poleVault;
            return this;
        }

        public AthleteResultBuilder javelinThrow(JavelinThrow javelinThrow) {
            this.javelinThrow = javelinThrow;
            return this;
        }

        public AthleteResultBuilder oneThousandFiveHundredMeter(OneThousandFiveHundredMeter oneThousandFiveHundredMeter) {
            this.oneThousandFiveHundredMeter = oneThousandFiveHundredMeter;
            return this;
        }

        public AthleteResult build() {
            return new AthleteResult(this);
        }

    }

}
