package ee.kuehnenagel.event;

import ee.kuehnenagel.enums.UnitType;

public final class OneHundredMeter implements TrackEvent {

    private static final double A = 25.4347;
    private static final double B = 18;
    private static final double C = 1.81;

    private final double p;

    public OneHundredMeter(double p) {
        this.p = p;
    }

    public UnitType getEventType() {
        return UNIT_TYPE;
    }

    public double getA() {
        return A;
    }

    public double getB() {
        return B;
    }

    public double getC() {
        return C;
    }

    @Override
    public double getP() {
        return this.p;
    }

    @Override
    public String toString() {
        return "<OneHundredMeter>" + getP() + "</OneHundredMeter>";
    }

}
