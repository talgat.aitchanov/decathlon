package ee.kuehnenagel.event;

import ee.kuehnenagel.enums.UnitType;

public interface Event {

    UnitType getEventType();

    double getA();

    double getB();

    double getC();

    double getP();

}
