package ee.kuehnenagel.event;

import ee.kuehnenagel.enums.UnitType;

public interface TrackEvent extends Event {

    UnitType UNIT_TYPE = UnitType.SECOND;

}
