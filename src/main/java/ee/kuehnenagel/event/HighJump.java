package ee.kuehnenagel.event;

import ee.kuehnenagel.enums.UnitType;

public final class HighJump implements FieldEvent {

    private static final double A = 0.8465;
    private static final double B = 75;
    private static final double C = 1.42;

    private final double p;

    public HighJump(double p) {
        this.p = p;
    }

    public UnitType getEventType() {
        return UNIT_TYPE;
    }

    public double getA() {
        return A;
    }

    public double getB() {
        return B;
    }

    public double getC() {
        return C;
    }

    @Override
    public double getP() {
        return this.p;
    }

    @Override
    public String toString() {
        return "<HighJump>" + getP() + "</HighJump>";
    }

}
