package ee.kuehnenagel.event;

import ee.kuehnenagel.enums.UnitType;

public final class JavelinThrow implements FieldEvent {

    private static final double A = 10.14;
    private static final double B = 7;
    private static final double C = 1.08;

    private final double p;

    public JavelinThrow(double p) {
        this.p = p;
    }

    public UnitType getEventType() {
        return UNIT_TYPE;
    }

    public double getA() {
        return A;
    }

    public double getB() {
        return B;
    }

    public double getC() {
        return C;
    }

    @Override
    public double getP() {
        return this.p;
    }

    @Override
    public String toString() {
        return "<JavelinThrow>" + getP() + "</JavelinThrow>";
    }

}
