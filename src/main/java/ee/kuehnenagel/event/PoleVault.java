package ee.kuehnenagel.event;

import ee.kuehnenagel.enums.UnitType;

public final class PoleVault implements FieldEvent {

    private static final double A = 0.2797;
    private static final double B = 100;
    private static final double C = 1.35;

    private final double p;

    public PoleVault(double p) {
        this.p = p;
    }

    public UnitType getEventType() {
        return UNIT_TYPE;
    }

    public double getA() {
        return A;
    }

    public double getB() {
        return B;
    }

    public double getC() {
        return C;
    }

    @Override
    public double getP() {
        return this.p;
    }

    @Override
    public String toString() {
        return "<PoleVault>" + getP() + "</PoleVault>";
    }

}
