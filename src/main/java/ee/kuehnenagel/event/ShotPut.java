package ee.kuehnenagel.event;

import ee.kuehnenagel.enums.UnitType;

public final class ShotPut implements FieldEvent {

    private static final double A = 51.39;
    private static final double B = 1.5;
    private static final double C = 1.05;

    private final double p;

    public ShotPut(double p) {
        this.p = p;
    }

    public UnitType getEventType() {
        return UNIT_TYPE;
    }

    public double getA() {
        return A;
    }

    public double getB() {
        return B;
    }

    public double getC() {
        return C;
    }

    @Override
    public double getP() {
        return this.p;
    }

    @Override
    public String toString() {
        return "<ShotPut>" + getP() + "</ShotPut>";
    }

}
