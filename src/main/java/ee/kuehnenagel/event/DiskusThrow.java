package ee.kuehnenagel.event;

import ee.kuehnenagel.enums.UnitType;

public final class DiskusThrow implements FieldEvent {

    private static final double A = 12.91;
    private static final double B = 4;
    private static final double C = 1.1;

    private final double p;

    public DiskusThrow(double p) {
        this.p = p;
    }

    public UnitType getEventType() {
        return UNIT_TYPE;
    }

    public double getA() {
        return A;
    }

    public double getB() {
        return B;
    }

    public double getC() {
        return C;
    }

    @Override
    public double getP() {
        return this.p;
    }

    @Override
    public String toString() {
        return "<DiskusThrow>" + getP() + "</DiskusThrow>";
    }

}
