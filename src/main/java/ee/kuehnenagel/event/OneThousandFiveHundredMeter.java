package ee.kuehnenagel.event;

import ee.kuehnenagel.enums.UnitType;

public final class OneThousandFiveHundredMeter implements TrackEvent {

    private static final double A = 0.03768;
    private static final double B = 480;
    private static final double C = 1.85;

    private final double p;

    public OneThousandFiveHundredMeter(double p) {
        this.p = p;
    }

    public UnitType getEventType() {
        return UNIT_TYPE;
    }

    public double getA() {
        return A;
    }

    public double getB() {
        return B;
    }

    public double getC() {
        return C;
    }

    @Override
    public double getP() {
        return this.p;
    }

    @Override
    public String toString() {
        return "<OneThousandFiveHundredMeter>" + getP() + "</OneThousandFiveHundredMeter>";
    }

}
