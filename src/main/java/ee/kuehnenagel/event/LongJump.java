package ee.kuehnenagel.event;

import ee.kuehnenagel.enums.UnitType;

public final class LongJump implements FieldEvent {

    private static final double A = 0.14354;
    private static final double B = 220;
    private static final double C = 1.4;

    private final double p;

    public LongJump(double p) {
        this.p = p;
    }

    public UnitType getEventType() {
        return UNIT_TYPE;
    }

    public double getA() {
        return A;
    }

    public double getB() {
        return B;
    }

    public double getC() {
        return C;
    }

    @Override
    public double getP() {
        return this.p;
    }

    @Override
    public String toString() {
        return "<LongJump>" + getP() + "</LongJump>";
    }

}
