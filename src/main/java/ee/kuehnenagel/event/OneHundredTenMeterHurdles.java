package ee.kuehnenagel.event;

import ee.kuehnenagel.enums.UnitType;

public class OneHundredTenMeterHurdles implements TrackEvent {

    private static final double A = 5.74352;
    private static final double B = 28.5;
    private static final double C = 1.92;

    private final double p;

    public OneHundredTenMeterHurdles(double p) {
        this.p = p;
    }

    public UnitType getEventType() {
        return UNIT_TYPE;
    }

    public double getA() {
        return A;
    }

    public double getB() {
        return B;
    }

    public double getC() {
        return C;
    }

    @Override
    public double getP() {
        return this.p;
    }

    @Override
    public String toString() {
        return "<OneHundredTenMeterHurdles>" + getP() + "</OneHundredTenMeterHurdles>";
    }

}
