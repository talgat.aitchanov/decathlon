package ee.kuehnenagel.event;

import ee.kuehnenagel.enums.UnitType;

public interface FieldEvent extends Event {

    UnitType UNIT_TYPE = UnitType.METER;

}
